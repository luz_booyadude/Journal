﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.IO;
using System.Globalization;
using System.Threading;

namespace Journal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string DATE = DateTime.Now.ToString("yyyy-MM-dd");
        private bool _UNSAVED = false;
        public bool UNSAVED
        {
            get { return _UNSAVED; }
            set
            {
                _UNSAVED = value;
                if (_UNSAVED == true)
                {
                    this.Title += "*";
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            this.Title = DATE;

            // Set a fixed date format.
            CultureInfo ci = CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.Name); ci.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            Thread.CurrentThread.CurrentCulture = ci;

            datePicker.SelectedDate = DateTime.Today;

            string content = DB.QueryScalar("select content from journal where date ='" + DATE + "'").ToString();
            journalContentBox.Document.Blocks.Clear();
            if (content != "-1")
            {
                journalContentBox.AppendText(content);
            }
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var datepicker = sender as DatePicker;
            DateTime? date = datepicker.SelectedDate;
            if (date == null)
            {
                // ... A null object.
                this.Title = "No date";
            }
            else
            {
                // ... No need to display the time.
                this.Title = date.Value.ToShortDateString();
                DATE = this.Title;
            }

            string content = DB.QueryScalar("select content from journal where date ='" + DATE + "'").ToString();
            journalContentBox.Document.Blocks.Clear();
            if(content != "-1")
            {
                journalContentBox.AppendText(content);
            }
        }

        private void journalContentBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                SaveNotes();
                return;
            }       

            if (!UNSAVED)
            {
                UNSAVED = true;
            }
        }

        private void SaveNotes()
        {
            if(UNSAVED)
            {
                string content = new TextRange(journalContentBox.Document.ContentStart, journalContentBox.Document.ContentEnd).Text.Replace("'", "''");
                DB.Query("REPLACE INTO `journal` (date, content) values ('" + DATE + "', '" + @content + "')");
                UNSAVED = false;
                this.Title = DATE;
            }
        }

        private void btnToday_Click(object sender, RoutedEventArgs e)
        {
            SaveNotes();
            datePicker.SelectedDate = DateTime.Today;
        }

        private void btnPreviousDay_Click(object sender, RoutedEventArgs e)
        {
            SaveNotes();
            datePicker.SelectedDate = datePicker.SelectedDate.Value.AddDays(-1);
        }

        private void btnNextDay_Click(object sender, RoutedEventArgs e)
        {
            SaveNotes();
            datePicker.SelectedDate = datePicker.SelectedDate.Value.AddDays(1);
        }

        private void journalContentBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var newPointer = journalContentBox.Selection.Start.InsertLineBreak();
                journalContentBox.Selection.Select(newPointer, newPointer);
                e.Handled = true;
                return;
            }
            if (e.Key == Key.Left && Keyboard.Modifiers == ModifierKeys.Control)
            {
                SaveNotes();
                datePicker.SelectedDate = datePicker.SelectedDate.Value.AddDays(-1);
                return;
            }
            if (e.Key == Key.Right && Keyboard.Modifiers == ModifierKeys.Control)
            {
                SaveNotes();
                datePicker.SelectedDate = datePicker.SelectedDate.Value.AddDays(1);
                return;
            }
            if (e.Key == Key.Down && Keyboard.Modifiers == ModifierKeys.Control)
            {
                SaveNotes();
                datePicker.SelectedDate = DateTime.Today;
                return;
            }
        }
    }
}
